> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 - Web Applications development

## Jacob Robertson

### Class Number Requirements:

- Demonstrate the ability to program and deploy client/server-side scripts
- Describe web-based input and output processes
- Demonstrate web application to data source connectivity
- Develop a dynamic web application using a mix of front-end and back-end web technologies
- Employ OOP techniques, as well as business logic using a strongly typed language.
- Create client-and server-side data validation.

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide Screenshots of installations
    - Create bitbucket repo
    - complete bitbucket tutorials
    - provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Develop and deploy webapp
    - Write a welcome page
    - Write/compile servlet
    - Write Datebase servlet

3. [A3 README.md](a3/README.md "My A3 README.md file")

    - Develop a ERD
    - Incude atleast 10 records in each table
    - Provide bitbucket read only access

4. [A4 README.md](a4/README.md "My A4 README.md file")

    - Edit and compile CustomerServlet.java and Customer.java
    - Screenshots of Assignment
        - A4 failed validation
        - A4 passed validation
    - Canvas Links: Bitbucket repo

5. [A5 README.md](a5/README.md "My A5 README.md file")

    - Edit A5 webpage
    - Show data was entered into database that was createrd in a A3
    - Screenshots of Assignment
        - Screenshots of A5 Valid User Form Entry
        - Screenshots of A5 Passed Validation
        - Screenshots of Associatede database entry
    - Canvas Links: Bitbucket repo
    
4. [P1 README.md](p1/README.md "My P1 README.md file")

    - Change photos on Home page carousel from robot.img
    - Add jQuery validation and regular expressions
    - Research validation codes

5. [P2 README.md](p2/README.md "My P2 README.md file")

    - Edit and complile CustomerServlet.java, CustomerDB.java.
    - Edit customers.jsp and modify.jsp
    - Screenshots of Assignment
    - Canvas Links: Bitbucket repo
