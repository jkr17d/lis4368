
# LIS 3268

## Jacob Robertson

### Assignment 3 Requirements:

1. Entity Relationship Diagram (ERD()

2. Include data (atleast 10 recrod each table)

3. Provide Bitbcuket read-only access to repo (language SQL) MUST include README.md using Markdown syntax, and include links to the following files (from README.md):
    - docs folder: a3.mwb and a3.sql
    - img folderL a3.png (export a3.mwb file as a3.png)
    - README.md (MUST display a3.png ERD)
    - Bitbucket repo

#### README.md file should include the following items:

1. Screenshot of a3 ERD that links to the image



#### Assignment Screenshots:

*Screenshot of a3 ERD:

![a3 ERD Screenshot](img/a3.png)

*A3 docsL a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format") 

[A3 SQL file](docs/a3.sql "A3 SQL Script")

