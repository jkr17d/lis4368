
# LIS 3268

## Jacob Robertson

### Assignment 2 Requirements:

1. Provide Bitbucketread-only access to lis4368repo, includethe README.md, using Markdownsyntax. (README.mdmust also include screenshots as per above and below.)(DO NOT create README in Bitbucket—ALWAYSdo it locally, then push it to Bitbucket.)

2. FSU’s Learning Management System:include lis4368Bitbucketrepolink

#### README.md file should include the following items:

* Assessment links:
    - http://localhost:9999/hello
    - http://localhost:9999/hello/HelloHome.html
    - http://localhost:9999/hello/sayhello
    - http://localhost:9999/hello/querybook.html


* Only *one* screenshot of the query resultsfrom the following link(see screenshot below): http://localhost:9999/hello/querybook.html


#### Assignment Screenshots:

*Screenshot of http://localhost:3242/hello:

![hello Screenshot](img/hello.png)

*Screenshot of http://localhost:3242/hello/HelloHome.html

![HelloHome.html Screenshot](img/HelloHome.png)

*Screenshot of http://localhost:3242/hello/sayhello

![sayhello.html Screenshot](img/sayhello.png)

*Screenshot of http://localhost:3242/hello/querybook.html

![querybook.html Screenshot](img/querybook.png)

*Screenshot of query results:

![querybook.html Screenshot](img/queryresults.png)