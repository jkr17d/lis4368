
# LIS 3268

## Jacob Robertson

### Project 1 Requirements:

1. Change photos on Home page carousel from robot.img

2. Add the following jQuery validation and regular expressions:
    - fname
    - lname
    - street
    - city
    - state
    - zip
    - phone
    - email
    - balance
    - total_sales

3. Research what the following validation code does:
    - valid: 'fa fa-check',
    - invalid: 'fa fa-times',
    - validating: 'fa fa-refresh'
 

#### README.md file should include the following items:

1. Screenshot of LIS4368 Portal (Main/Splash Page)

2. Screenshot of Failed Validation

3. Screenshot of Passed Validation



#### Assignment Screenshots:

*Screenshot of failed validation:*

![P1 failed validation Screenshot](img/xvalidation.png)

*Screenshot of passed validation:*

![P1  passed validation screenshot](img/validation.png) 

*Screenshot of changed carousel pictures:*

![P1 carousel picture](img/pics.png)

