
# LIS 3268

## Jacob Robertson

### Assignment 4 Requirements:

Deliverables:

Four Parts:

1. Edit and compile CustomerServlet.java and Customer.java

2. Screenshots of Project
    - A4 passed/failed validations

3. Canvas Links: Bitbucket repo
4. http://localhost:9999/lis4368/customerform.jsp?assign_num=a4

#### README.md file should include the following items:

1. Screenshot of A4 Failed Validation
2. Screenshot of A4 Passed Validation



#### Assignment Screenshots:

*Screenshot of Failed Validation:

![A4 failed validation](img/failed.png)

*Screenshot of Passed Validation:

![A4 passed validation](img/passed.png)

