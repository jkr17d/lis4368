
# LIS 3268

## Jacob Robertson

### Assignment 5 Requirements:

Deliverables:

    - Edit A5 webpage
    - Show data was entered into database that was createrd in a A3
    - Screenshots of Assignment
    - Canvas Links: Bitbucket repo

#### README.md file should include the following items:

1. Screenshots of A5 Valid User Form Entry
2. Screenshots of A5 Passed Validation
3. Screenshots of Associatede database entry


#### Assignment Screenshots:

*Screenshots of A5 Valid User Form Entry:

![A5 Valid User Form Entry](img/validform.png)

*Screenshots of A5 Passed Validation:

![A5 Passed Validation](img/passedvalidation.png)

*Screenshots of Associated database entry

![A5 Associated database entry](img/databaseentry.png)