import java.util.*;

public class NumberSwap {
	public static void main(String[] args) {
    	Scanner scnr = new Scanner(System.in);
    	int input = 0, input2 = 0, flag, temp;
    	
    	System.out.println("Program swaps two integers.");
    	System.out.println("Note: Program checks for integers and non-numeric values\n");
    	
    	do{
    	    System.out.print("Please enter first number: ");
    	    try {
    	       input = scnr.nextInt();
    	       flag = 0;
    	    } catch(InputMismatchException e) {
    	       System.out.println("Not valid integer!");
    	       flag = 1;
    	       System.out.print("\nPlease try again. ");
    	       scnr.next();
    	    }
    	}while(flag != 0);
    	
    	System.out.println("");
    	
    	do{
    	    System.out.print("Please enter second number: ");
    	    try {
    	       input2 = scnr.nextInt();
    	       flag = 0;
    	    } catch(InputMismatchException e) {
    	       System.out.println("Not valid integer!");
    	       flag = 1;
    	       System.out.print("\nPlease try again. ");
    	       scnr.next();
    	    }
    	}while(flag != 0);
    	
    	System.out.println("\nBefore swapping");
    	System.out.println("num1 = " + input);
    	System.out.println("num2 = " + input2 + "\n");
    	
    	temp = input; //holding input value
    	input = input2; //changing input value to input2
    	input2 = temp;
    	
    	System.out.println("After swapping");
    	System.out.println("num1 = " + input);
    	System.out.println("num2 = " + input2 + "\n");
	}
}