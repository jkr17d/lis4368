
# LIS 3268

## Jacob Robertson

### Project 2 Requirements:

Deliverables:

    - Edit and complile CustomerServlet.java, CustomerDB.java.
    - Edit customers.jsp and modify.jsp
    - Screenshots of Assignment
    - Canvas Links: Bitbucket repo

#### README.md file should include the following items:

1. Screenshots of P2 Valid User Form Entry
2. Screenshots of P2 Passed Validation
3. Screenshots of P2 Display Data
4. Screenshots of P2 Modify Form
5. Screenshots of P2 Modified Data
6. Screenshots of P2 Delete Warning
7. Screenshots of P2 Associated Database Changes


#### Assignment Screenshots:

*Screenshots of P2 Valid User Form Entry:

![P2 Valid User Form Entry](img/validentry.png)

*Screenshots of P2 Passed Validation:

![P2 Passed Validation](img/passedvalidation.png)

*Screenshots of P2 Display Data:

![P2 Display Data](img/displaydata.png)

*Screenshots of P2 Modify Form:

![P2 Modify Form](img/modifyform.png)

*Screenshots of P2 Modified Data:

![P2 Modified Data](img/modifieddata.png)

*Screenshots of P2 Delete Warning:

![P2 Delete Warning](img/deletewarning.png)

*Screenshots of P2 Associated Database Changes:

![P2 Associated Database Changes](img/database.png)